package product

import "github.com/jinzhu/gorm"

//Product type: Modelo usado por Gorm para el tratamiento de la db.
type Product struct {
	gorm.Model
	Code  string
	Price uint
}
