package product

//ProductDTO type: Lo usaremos para generar los mapas que devolveremos
type ProductDTO struct {
	ID    uint   `json:"id,string,omitempty"`
	Code  string `json:"code"`
	Price uint   `json:"price,string"`
}
