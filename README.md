# Golang-GinGonic-Gorm-Wire-MySql ApiRest

Very simple example to create a mysql or whatever db technology ApiRest with Golang as a base language, GinGonic as a Go framework, Gorm as an ORM library and Wire as a Compilation-Time Dependency Injection.

- [Gorm](https://gorm.io/)
- [Wire](https://blog.golang.org/wire)

# Deploy
1. Clone this repo
2. Run the docker-compose up --build
3. Atach this coment at the top of the /test/wire.go file:(Remember to leave a blank line under the comment)
-       //+build wireinject 
    3.1 This an example of the wire.go file:
```      
        //+build wireinject  

        package main

        import (
            "Go_Gingonic_Server/product"

            "github.com/google/wire"
            "github.com/jinzhu/gorm"
        )

        func initProductAPI(db *gorm.DB) product.ProductAPI {
            wire.Build(product.ProvideProductRepostiory, product.ProvideProductService, product.ProvideProductAPI)

            return product.ProductAPI{}
        }
```
4. Traefik creates a domain called test.localhost. To test the API insert in postman or in your browser the following lines:
    
    1. Find all products test.localhost:3010/products
    2. Find a product test.localhost:3010/products/1
    3. Create a new product test.localhost:3010/products (code=p1 price=10)
    4. Update a product test.localhost:3010/products/1 (code=p1 price=100)
    5. Delete a product test.localhost:3010/products/1

# How It Works

```
    ├── test
    ├────── go.mod  //all dependencies needed by the example
    ├────── go.sum
    ├────── main.go //Initialize the database, obtain all the 
    ├                  injections and creates the app router
    ├────── wire.go //Provide information on which suppliers 
    ├                  will be used to create the event that we 
    ├                  will use to deal with the imports
    ├────── wire_gen.go //the event generated in compilation-time
    ├────── product
    ├────────── product_api.go //Deal with the connection between the api 
    ├                             services and the router. Uses the mapper 
    ├                             to format the responses to the frontend
    ├────────── product_service.go // Creates a bridge between the 
    ├                                  repository and the api.
    ├────────── product_repository.go // Performs all operations related 
    ├                                     to the database 
    ├────────── product_mapper.go //formats the responses with the dto 
    ├                                and the gorm models structures
    ├────────── product_dto.go //structures used in the responses send to 
    ├                             the frontend
    ├────────── product.go // structure used by gorm
    
```
